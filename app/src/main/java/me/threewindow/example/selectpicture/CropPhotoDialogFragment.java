package me.threewindow.example.selectpicture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.support.v4.provider.DocumentFile;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;

public class CropPhotoDialogFragment extends DialogFragment {

    public interface OnCropListener {
        void OnCrop(Bitmap bitmap);
    }

    OnCropListener l;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crop, container, false);
        v.findViewById(R.id.select_photo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectPhotoButtonClick();
            }
        });

        v.findViewById(R.id.take_photo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTakePhotoButtonClick();
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnCropListener) {
            l = (OnCropListener) context;
        }
    }

    private int TAKE_CAMERA = 1;
    private int TAKE_GALLERY = 2;
    private int CROP = 3;

    private Uri albumPhotoUri; //  앨범에서 선택한 사진의 uri
    private Uri beforeCropPhotoUri; // 크롭에 전달할 사진의 uri

    @Getter
    private Uri afterCropPhotoUri; // 크롭이 진행된 이후의 사진의 uri

    public  void onSelectPhotoButtonClick() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, TAKE_GALLERY);
    }

    public void onTakePhotoButtonClick() {
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            return;
        }

        // Continue only if the File was successfully created
        if (photoFile != null) {


            // kitkat, jelly bean
            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                beforeCropPhotoUri = Uri.fromFile(photoFile);
            }
            // Oreo, PIE
            else {
                beforeCropPhotoUri = FileProvider.getUriForFile(getActivity(),
                        getActivity().getPackageName(),
                        photoFile);
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, beforeCropPhotoUri);
            startActivityForResult(intent, TAKE_CAMERA);

        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        //storageDir = getCacheDir();

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    private Uri getSelectedImageFileUri(Uri uri) {
        try {
            //DocumentFile file = DocumentFile.fromFile(new File(uri.getPath()));
            InputStream is = getActivity().getContentResolver().openInputStream(uri);

            File beforeCropPhotoFile = createImageFile();
            OutputStream out = getActivity().getContentResolver().openOutputStream(Uri.fromFile(beforeCropPhotoFile));

            byte[] b = new byte[1024];
            int len = 0 ;
            while((len = is.read(b)) > 0) {
                out.write(b,0, len);
            }
            out.flush();

            out.close();
            is.close();

            // Files.copy(is, beforeCropPhotoFile.toPath(), StandardCopyOption.REPLACE_EXISTING); // after java 7 , api 19

            return FileProvider.getUriForFile(getActivity(),
                    getActivity().getPackageName(),
                    beforeCropPhotoFile);
        }
        catch (IOException e) {
            return uri;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == TAKE_GALLERY) {
                if (data != null) {
                    albumPhotoUri = data.getData();

                    // kitkat, jellybean
                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                        beforeCropPhotoUri = albumPhotoUri;
                    }
                    // kitkat, oreo
                    else {
                        beforeCropPhotoUri = getSelectedImageFileUri(albumPhotoUri);
                    }
                    cropImage(beforeCropPhotoUri);
                }
            }
            else if (requestCode == TAKE_CAMERA) {
                cropImage(beforeCropPhotoUri);

            }
            else if (requestCode == CROP) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), this.afterCropPhotoUri);
                    l.OnCrop(bitmap);
                }
                catch(IOException e) {
                    // todo : 예외 처리
                }

                dismiss();
            }
        }
        else {
            dismiss();
        }
    }


    protected  void cropImage(Uri photoURI) {

        File cropPhotoFile = null;
        try {
            cropPhotoFile = createImageFile();
        }
        catch (IOException e) {
            return ;
        }

        afterCropPhotoUri = Uri.fromFile(cropPhotoFile);

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(photoURI, "image/*");

        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, afterCropPhotoUri);

        startActivityForResult(intent, CROP);
    }
}
