package me.threewindow.example.selectpicture;

import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements CropPhotoDialogFragment.OnCropListener {

    private ImageView imageView;
    private ImageView thumbnailImageView;
    private TextView selectImageInfoTextView;
    private EditText thumbnailImageResEditText;
    private TextView thumbnailImageInfoTextView;
    private CropPhotoDialogFragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.select_image_view);
        thumbnailImageView = findViewById(R.id.thumbnail_image_view);
        selectImageInfoTextView = findViewById(R.id.select_image_info_text_view);
        thumbnailImageResEditText = findViewById(R.id.thumbnail_image_res_edit_text);
        thumbnailImageInfoTextView = findViewById(R.id.thumbnail_image_info_text_view);
        fragment = new CropPhotoDialogFragment();
   }



    public void onClickCropPhotoButtonClick(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragment.show(fragmentTransaction, "dialog");
    }

    @Override
    public void OnCrop(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);

        selectImageInfoTextView.setText(String.format("Croped image [%d] x [%d]", bitmap.getWidth(), bitmap.getHeight()));
    }

    public void onClickMakeThumbButtonClick(View view) {
        String res = thumbnailImageResEditText.getText().toString();

        if (res.length() > 0 && imageView.getDrawable() != null ) {
            int resoulution = Integer.parseInt(res);

            Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            Bitmap thumbnailBitmap = Bitmap.createScaledBitmap(bitmap, resoulution, resoulution, false);

            thumbnailImageView.setImageBitmap(thumbnailBitmap);
            thumbnailImageInfoTextView.setText(String.format("Thumbnail image [%d] x [%d]", thumbnailBitmap.getWidth(), thumbnailBitmap.getHeight()));
        }
    }
}
